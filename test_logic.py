import unittest
import json
from src.logic.playlist import Playlists
from src.logic.song import Song, SearchForSongs

class TestSong(unittest.TestCase):
    ''' Class for testing songs'''

    def test_song(self):
        ''' A function that tests all major functionalities for song'''
        song = Song()

        self.assertIsNone(song.name)
        self.assertIsNone(song.artist)
        self.assertIsNone(song.mood)
        self.assertIsNone(song.genre)

        song.set_name('Wannabe')
        self.assertEqual(song.name, 'Wannabe')
        self.assertIsNotNone(song.name)

        song.set_artist('Spice Girls') # Happy Path
        self.assertEqual(song.artist, 'Spice Girls')
        self.assertIsNotNone(song.artist)

        song.set_genre('pop')
        self.assertEqual(song.genre, 'pop')
        self.assertIsNotNone(song.genre)

        song.set_mood('happy')
        self.assertEqual(song.mood, 'happy')
        self.assertIsNotNone(song.mood)

        song.add_time

    def test_search_for_songs(self):
        sfs = SearchForSongs()


class TestPlaylist(unittest.TestCase):
    ''' Class for testing different functionlites for playlists '''
   
    def test_create_playlists(self):
        ''' A function that tests that a playlist can be created '''
        pl = Playlists()

        result = pl.create_playlist('test1', 'pop', 'happy')  # Happy path
        self.assertTrue(result)  # Expecting True for valid creation

        result = pl.create_playlist('test1', 'rock', 'happy')  # Name already exists (is allowed)
        self.assertTrue(result)  # Expecting True for valid creation

        result = pl.create_playlist('test2', 'rock', 'happy')  
        self.assertTrue(result)  # Expecting True for valid creation

        result = pl.create_playlist('test2', 'rock')  
        self.assertFalse(result)  # Expecting False for invalid creation

    def test_view_playlists(self):
        ''' A function that tests if correct playlists are returned'''
        pl = Playlists()
        playlist_names = pl.view_all_playlists()  # Call the view_all_playlists function and get the playlist names

        # Updated expected_playlist_names to match the current behavior
        expected_playlist_names = ['test1', 'test2']  # Test data
        for name in expected_playlist_names:
            self.assertIn(name, playlist_names)  # Check if each expected name is in the actual list

    def test_get_last_playlist_id(self):
        ''' A function that checks if the get last id of playlist function works'''
        pl = Playlists()

        last_id = pl.get_last_playlist_id()
        self.assertIsInstance(last_id, int)

    def test_adding_song_playlist(self):
        ''' A function that checks if adding song to a playlist works properly'''
        pl = Playlists()
        pl.add_song_to_playlist(1,[1,2])

        with open("src/files/playlists.json", 'r') as file:
            playlist_data = json.load(file)

        for item in playlist_data:
            if item["id"] == 1:
                self.assertEqual(item["songs"], [1,2])

    def test_deleting_song_playlist(self):
        ''' A function that checks if deleting a song from a playlist works'''
        pl = Playlists()
        pl.delete_song_from_playlist(1,2)

        with open("src/files/playlists.json", 'r') as file:
            playlist_data = json.load(file)

        for item in playlist_data:
            if item["id"] == 1:
                self.assertNotIn(2, item["songs"])

    def test_change_playlist_genre(self):
        ''' A function that tests changing a playlist genre'''
        pl = Playlists()
        pl.create_playlist('test3', 'pop', 'happy')  # Create a playlist for testing
        pl.change_playlist_genre('test3', 'rock')  # Call the function to change the genre

        with open("src/files/playlists.json", 'r') as file:
            playlist_data = json.load(file)

        for playlist in playlist_data:
            if playlist["name"] == 'test3':
                self.assertEqual(playlist["genre"], 'rock')
                

    def test_change_playlist_mood(self):
        pl = Playlists()
        pl.create_playlist('test4', 'pop', 'happy')  # Create a playlist for testing
        pl.change_playlist_mood('test4', 'sad')  # Call the function to change the mood

        with open("src/files/playlists.json", 'r') as file:
            playlist_data = json.load(file)

        for playlist in playlist_data:
            if playlist["name"] == 'test4':
                self.assertEqual(playlist["mood"], 'sad')

    def test_load_songs(self):
        pl = Playlists()
        songs = pl.load_songs()
        self.assertIsInstance(songs, list)

    def test_load_playlists(self):
        pl = Playlists()
        playlists = pl.load_playlists()
        self.assertIsInstance(playlists, list)

    @classmethod
    def tearDownClass(cls):
        ''' Function to delete everything that the tests created'''
        with open("src/files/playlists.json", 'w') as file:
            file.write(json.dumps([]))

if __name__ == '__main__':
    unittest.main()

