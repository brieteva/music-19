# Group 19 - Decision protocal

## Sprint 1

### 30.08.23

All the group showed up in the first class and started to get to know each other. As a group we went through our options and decided to go with mood music as our Project. The reason we chose mood music is because it reflected more of our interests. Each one of us listens to music and therefore have their own thoughts and inputs on how we can make the project in the best way possible. This way everyone can contribute equally to the project. We also have decided to create 16 user stories/issues, to fulfil the important requirements for our project.

### 03.09.23

The group met in school to work on the project. We interpereted the requirements and User stories as almost the same thing and created issues on GitLab following a simple format. Together we decided what we wanted as user stories and had one person create the issues as we went on. After all the user stories were created, we then created the necessary requirements based off the stories and ranked them based on their priorities. We decided to create personas for our users so that we could better understand what our expected users would want from our Music program. In total we made four different personas for our expected users. Three different scenarios were created that should cover most of our requirements for the project. The requirements are connected to the scenarios in their respected table. The Project Overview has been successfully created and mostly finished especially from the Sprint 1 requirements.

### 06.09.23

Today, with Stefán and Aron abroad, only four team members were present. We focused on finalizing Sprint 1 tasks for our assignment. During this session, we made necessary refinements and defined our application's scope. We decided on four music genres, exclusive U.S. artist content, and a target user base of Reykjavík University employees and students. Our collaboration was effective, resulting in project completion and satisfaction.

### 13.09.23

## Scrum Master Stefán

Today the group chose a scrum master. Stefan was elected. Aron had to leave early. We focused on how we wanted to split up the work for the user stories. We created a milestone to help keep track of the current sprint.
A decision was also made that the current scrum master should keep track of the decision protocol for the current sprint. this way the job gets split up between the team.

## User Stories

with 14 user stories we split up the group into pairs and each pair can do one user story per sprint for the next 4 sprints. We chose 4 user stories to start with this sprint, Go to homepage, Create a playlist, View all playlists, song view.
the groups and their assigned user stories:
Bríet & Hulda: US5 create a playlist & US8 view a playlist
Stefán & Aron: US16 Song View
Snorri & Alessandro: US7 Homepag

## Unit Tests

We decided that each pair should also create the unit tests for their user stories. this way the devs of each user storie can create the optimal tests for their code. We will download and use https://docs.python.org/3/library/unittest.html

## Naming Conventions

variables: snake casing, all lowercase, spacing using underscore (example: test_variable)
constants: snake casing, all uppercase, spacing using underscore (example: TEST_CONSTANT)
function: snake casing, all lowercase, spacing using underscore (example: test_function())
classes: Capitalized, all words capitalized, no spacing (example: TestClass)
file names: Capitalized, all words capitalized, no spacing (example: TestFile.file)

## Sprint 2

### 20.09.23

The group gathered together in class, unfortunate that Shalini was absent. There was no substitute so we had to work through it on our own. We struggled a bit with gRPC because of this. Everyone has started on their issues. We had strategically picked issues that were imported ground work but that were distinct enough to not cause merge errors when we merge the branches.

### 26.09.23

Today the group had a quick meeting with full attendance. We went over what everyone had done throughout the sprint and we are pleased with everyones performance. We managed to hit our 3 issue goal for the sprint and even surpassed it by completing 4 User Stories. That includes US16 SongView, US search for song, US5 View Playlist, US7 Login Page & US7 Home Page.

### Current status at the end of sprint 2.

We have a working home and login page as the foundation of the app. We completed SongView and View Playlist which included creating a foundation class for a song for the songview to use & a foundational class for a playilist to view all the created playlists. We also added a search function for songs so that you can quickly find a selected song.

In conclusion we are well on our way with the groundwork for the project. The rest of the issues are dependant on these fundamentals and should be easier to implement with what we have got.

### 27.09.23 - Scrum master Bríet

As we did for sprint 2, we met in class today and got to work on the new DoD. Considering that several directories and classes had been generated twice, we also worked on aligning our code. Before we started development, we did not discuss who merges merge requests after they have been approved, therefore we chose to follow the rule that the creator will always merge their own merge requests going forward. Along with introducing gRPC, we also worked on creating a test coverage report.

### 28.09.23

An email was sent out today by the scrum master to group 20 that contained all emails of the group members, for a future code review.

### 04.10.23

The group met in class and worked on getting the coverage report percentage increased.

### Current status at the end of sprint 3

The group finished 3 user stories in which they pair programmed. Aron and Stefán created implemented a way to get information about songs in playlist as well as playing song in playlist in the correct order. They also changed the files from csv to json along with modifying the appriate code. Alessandro and Snorri made a function that orders the json files by mood and tests for that functionality. They also created a new ui file for playlist creation that uses the functionality of playlist from the logic layer, as well as minor changes. Bríet and Hulda developed a way to add songs to playlist and delete song to playlist. In addition to fixing previos tests that were failing they also created new tests for this functionality. The group increased the coverage report percentage this sprint so it's above 50% as desired by the product manager.

### 11.10.23 - Scrum Master Snorri

Similar to all of the other sprints we met in class and worked on the new Dod. We split assingments between us and started thinking about how we wanted to continue on with our program. We wanted to start piecing together all of the pieces that make up the program so it can run smoothly. We also started to think about how to implement the weather API as well as adding more GRPC and a user story.


### 18.10.23 
We all met together and asked questions about the weather API. Everyone was sure on what they were doing and continued working on that.

## Sprint 5

### 25.10.23 - Scrum Master Hulda
The group met in class to start on the final sprint of the semester, sprint 5. We split tasks between us in the same groups of two like we have been doing in the other sprints. Bríet and Hulda took the task to add CI/CD to our GitLab project as well as adding GitLab Badges. Alessandro and Snorri are doing the task to finish setting up the project with grpc. Aron and Stefán are taking two tasks: User story 11 to skip a song and User Story 17 to recommend a song based on weather.


### 01.11.23
The group met in class and each group continued working on their task for this sprint.  
