# This is a project by group 19 called music-19

## Members of this group and authors of this project are:

Alessandro Manfrini (alessandrom22@ru.is)
Aron Birgir Tryggvason (aront22@ru.is)
Bríet Eva Gísladóttir (briet20@ru.is)
Hulda Ösp Ágústsdóttir (huldaa20@ru.is)
Snorri Már Gunnarsson (snorri22@ru.is)
Stefán Foelsche Arnarsson (stefana22@ru.is)

## How to run the project

Begin with cloning the project

### How to run up server

1. Navigate to the logic folder
2. From there navigate to the proto folder
3. Run the following command: "python3 server.py"

### How to run up the client

1. Navigate to the main folder "Music-19"
2. Run the following command: "python3 client.py"

## How to run the tests

1. Run the following commands and be located in the music-19 folder:
   coverage run -m unittest
   coverage report
   coverage html
