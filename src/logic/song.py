import json;

class NotFoundException(Exception):
    pass
class Song:
    def __init__(self) -> None:
        self.songs_filepath = "src/files/songs.json"
        self.id = ""
        self.name = None
        self.artist = None
        self.mood = None
        self.genre = None
        self.duration = 20000 # int in miliseconds
        self.timePlayed = 1000 # int in miliseconds

    def set_name(self, name):
        ''' Sets the name of the song'''
        self.name = name

    def set_mood(self, mood):
        ''' Sets the mood of the song'''
        self.mood = mood
    
    def set_genre(self, genre):
        ''' Sets the genre of the song '''
        self.genre = genre

    def set_artist(self, artist):
        ''' Sets the artist of the song'''
        self.artist = artist

    def add_time(self, time):
        ''' Adds time to the time played for the song'''
        self.timePlayed += time

    def __str__(self) -> str:
        ''' Returns the name of the song'''
        return self.name
    
    def view_all_songs(self):
        ''' A function the returns all playlist in the json file '''
        song_names = []
        try:
            with open(self.songs_filepath, mode='r', encoding='utf-8') as jsonfile:
                songs = json.load(jsonfile)
                for song in songs:
                    if 'name' in song and 'artist' in song:
                        song_names.append(f'"{song["name"]}" - {song["artist"]}')
        except Exception as e:
            return []
        return song_names
    
class SearchForSongs:
    def __init__(self) -> None:
        self.filepath = "music-19/src/files/songs.json"

    def search_for_song(self, song_name):
        """Return songs with corresponding name"""
        song = song_name.lower()
        with open(self.filepath, "r", encoding="utf-8") as jsonfile:
            song_data = json.load(jsonfile)
            for song_info in song_data:
                if song_info["name"].lower() == song:
                    return song_info
        raise NotFoundException("Song Not Found")
