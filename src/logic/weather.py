import requests
import json

class Weather:
    def __init__(self) -> None:
       self.url = 'https://openweathermap.org'
       self.location = 'Reykjavík'
       self.API_KEY = '6839f8369713408992e4216771d7326e'
       self.weather = ''
       self.moods  = {
            'clear sky': 'happy',
            'few clouds': 'happy',
            'scattered clouds': 'happy',
            'broken clouds': 'happy',
            'shower rain': 'sad',
            'rain': 'sad',
            'thunderstorm': 'sad',
            'snow': 'sad',
            'mist': 'sad'
       }
       self.multi_forecast = None

    def get_weather(self, api):
        ''' A function that gets the weather from the api'''
        response = requests.get()
        if response.status_code == 200:
            self.formatted_print()
            return response
        return 0
    
    def get_current_weather(self, Location, API_KEY):
        ''' A function that gets the current weather from the api'''
        response = requests.get(f'https://api.openweathermap.org/data/2.5/weather?q={Location}&appid={API_KEY}')
        data = response.json()
        self.weather = data['weather'][0]['description']
    
    def get_four_day_forecast(self, Location, API_KEY):
        ''' A function that gets the four day forecast from the api'''
        response = requests.get(f'https://pro.openweathermap.org/data/2.5/forecast/hourly?q={Location}&appid={API_KEY}')
        MultiForecast = response.json()
        return MultiForecast
    
    def get_historical_forecast(self, HistoryPlace):
        ''' A function that gets the historical forecast from the api'''
        response = requests.get(f'https://api.openweathermap.org/data/2.5/onecall/timemachine?lat={HistoryPlace}&lon={HistoryPlace}&dt=1627790400&appid={API_KEY}')
        MultiForecast = response.json()
        self.multi_forecast = MultiForecast
    
    def formatted_print(self, obj):
        ''' A function that prints out the json object in a formatted way'''
        text = json.dumps(obj, sort_keys=True, indent=4)
        print(text)

    def get_mood(self, weather):
        ''' A function that returns the mood based on the weather'''
        return self.moods[weather]
    
    def set_mood(self, mood):
        ''' A function that sets the mood'''
        self.mood = mood

if __name__ == "__main__":
    W = Weather()
    W.get_current_weather(W.location, W.API_KEY)
