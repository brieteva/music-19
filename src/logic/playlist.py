import json;
import time;

class Playlists:
    ''' A class that handles all functionlity regarding playlists'''
    
    def __init__(self):
        self.playlist_filepath = "src/files/playlists.json"
        self.songs_filepath = "src/files/songs.json"
        self.playlist_id = self.get_last_playlist_id()
        self.genre = ["techno", "pop", "jazz", "rock"] # only genres that can be added
        self.mood = ["happy", "sad"]

    def get_last_playlist_id(self):
        ''' Function that gets the last/newest id from the json an returns it'''
        last_id = 0  # if there is nothing in the file
        with open(self.playlist_filepath, mode='r', encoding='utf-8') as jsonfile:
            playlists = json.load(jsonfile)
            for playlist in playlists:
                if 'id' in playlist and isinstance(playlist['id'], int):
                    last_id = max(last_id, playlist['id'])
            return last_id

        
    def create_playlist(self, name=None, genre=None, mood= None):
        '''A function that creates a playlist in the json with the provided information'''
        try:
            if not name or not genre or not mood:
                return False
            name = name.lower()
            genre = genre.lower()
            mood = mood.lower()
            if genre not in self.genre:
                return False
            if mood not in self.mood:
                self.mood.append(mood)
            

            else:
                with open(self.playlist_filepath, mode='r', encoding='utf-8') as jsonfile:
                    playlists = json.load(jsonfile)
                    new_playlist = {
                        "id": self.playlist_id + 1,
                        "name": name,
                        "genre": genre,
                        "mood": mood,
                        "songs": []
                    }
                    playlists.append(new_playlist)
                with open(self.playlist_filepath, mode='w', encoding='utf-8') as jsonfile:
                    json.dump(playlists, jsonfile, indent=4)
                self.playlist_id += 1  # update id for json
                return True
        except Exception as e:
            return False


    def view_all_playlists(self):
        ''' A function the returns all playlist in the json file '''
        playlist_names = []
        try:
            with open(self.playlist_filepath, mode='r', encoding='utf-8') as jsonfile:
                playlists = json.load(jsonfile)
                for playlist in playlists:
                    if 'name' in playlist:
                        playlist_names.append(playlist['name'])
            return playlist_names
        except Exception as e:
            return []
        
    def get_songs_in_playlist(self, playlist_name):
        ''' A helper function to play a playlist that finds all songs in the playlist '''
        song_id_list = []
        song_list = []
        # get id of songs in requested playlist
        try:
            with open(self.playlist_filepath, mode='r', encoding='utf-8') as playlists_jsonfile:
                playlists = json.load(playlists_jsonfile)
                for playlist in playlists:
                    if "name" in playlist and playlist["name"].lower() == playlist_name.lower():
                        song_id_list = playlist["songs"]
        except Exception as e:
            print(f"An exception occured: {e}")

        # get information on song with matching id            
        try:
            with open(self.songs_filepath, mode="r", encoding="utf-8") as songs_jsonfile:
                songs = json.load(songs_jsonfile)
                for id in song_id_list:
                    for song in songs:
                        if "id" in song and song["id"] == id["id"]:
                            song_list.append(song)
            return song_list
        
        except Exception as e:
            print(f"An exception occured: {e}")

    
    def play_playlist(self, playlist_name):
        ''' A function that plays a playlist '''
        songs = self.get_songs_in_playlist(playlist_name)
        playing_index = 0
        timer = songs[playing_index]['duration']
        self.now_playing(songs[playing_index])

        while (playing_index <= len(songs) - 1):
            time.sleep(timer)
            playing_index += 1
            timer = songs[playing_index]['duration']
            self.now_playing(songs[playing_index])

    def play_playlist_by_mood(self, mood):
        ''' A function that plays a playlist based on the mood'''
        with open(self.songs_filepath, mode="r", encoding="utf-8") as songs_jsonfile:
            songs = json.load(songs_jsonfile)
            for song in songs:
                if song["mood"] == mood:
                    self.now_playing(song)

    def play_playlist_based_on_weather(self, weather):
        ''' A function that plays a playlist based on the weather'''
        mood = weather.get_mood()
        self.play_playlist_by_mood(mood)

    def add_song_to_playlist(self,playlist_id,song_id_arr):
        ''' A function that adds a provided song to a playlist in the json file'''
        with open(self.playlist_filepath, 'r') as file:
            playlist_file = json.load(file)

        for item in playlist_file:
            if item["id"] == playlist_id:
                    item["songs"].extend(song_id_arr)

        with open(self.playlist_filepath, 'w') as file:
            json.dump(playlist_file, file, indent=2)

    def delete_song_from_playlist(self, playlist_id, song_id_to_delete):
        ''' A function that deletes a song from a playlist in the json file'''
        with open(self.playlist_filepath, 'r') as file:
            playlist_file = json.load(file)

        for item in playlist_file:
            if item["id"] == playlist_id:
                if song_id_to_delete in item["songs"]:
                    item["songs"].remove(song_id_to_delete)

        with open(self.playlist_filepath, 'w') as file:
            json.dump(playlist_file, file, indent=2)


    def sort_json_by_mood(self):
        ''' A function that sorts songs by mood'''
        sorted_data = sorted(self.playlist_filepath, key=lambda x: x['mood']) # Sort the JSON data by the "mood" key using lambda function
        return sorted_data

    def change_playlist_name(self, playlist_name, new_name):
        ''' A function that changes a playlist name'''
        with open(self.playlist_filepath, 'r') as file:
            data = json.load(file)

        # Find the playlist in the JSON data based on its ID
        for playlist in data:
            if playlist['name'] == playlist_name:
                playlist['name'] = new_name
                break  # Found the playlist, no need to continue searching

        # Write the updated JSON data back to the file
        with open(self.playlist_filepath, 'w') as file:
            json.dump(data, file, indent=4)


    def change_playlist_genre(self, playlist_name, new_genre):
        ''' A function that changes a playlist genre'''
        with open(self.playlist_filepath, 'r') as file:
            data = json.load(file)

        # Find the playlist in the JSON data based on its ID
        for playlist in data:
            if playlist['name'] == playlist_name:
                playlist['genre'] = new_genre
                break  # Found the playlist, no need to continue searching

        # Write the updated JSON data back to the file
        with open(self.playlist_filepath, 'w') as file:
            json.dump(data, file, indent=4)

    def change_playlist_mood(self, playlist_name, new_mood):
        '''A function that changes a playlist mood'''
        # Read the JSON data from the file
        with open(self.playlist_filepath, 'r') as file:
            data = json.load(file)

        # Find the playlist in the JSON data based on its ID
        for playlist in data:
            if playlist['name'] == playlist_name:
                playlist['mood'] = new_mood
                break  # Found the playlist, no need to continue searching

        # Write the updated JSON data back to the file
        with open(self.playlist_filepath, 'w') as file:
            json.dump(data, file, indent=4)

    def load_songs(self):
        with open(self.playlist_filepath, 'r') as file:
            songs = json.load(file)
        return songs

    # Load the playlists from playlists.json
    def load_playlists(self):
        with open(self.playlist_filepath, 'r') as file:
            playlists = json.load(file)
        return playlists

    # Save the updated playlists back to playlists.json
    def save_playlists(self,playlists):
        with open(self.playlist_filepath, 'w') as file:
            json.dump(playlists, file, indent=4)