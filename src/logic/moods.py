import json

class Moods:
    def __init__(self):
        self.moods_filepath = "src/files/moods.json"
        self.mood_id = self.get_last_mood_id()

    def get_last_mood_id(self):
        ''' Function that gets the last/newest id from the json an returns it'''
        last_id = 0  # if there is nothing in the file
        with open(self.moods_filepath, mode='r', encoding='utf-8') as jsonfile:
            moods = json.load(jsonfile)
            for mood in moods:
                if 'id' in mood and isinstance(mood['id'], int):
                    last_id = max(last_id, mood['id'])
            return last_id
        
    def create_mood(self, name=None):
        '''A function that creates a mood in the json with the provided information'''
        try:
            if not name:
                return False

            else:
                with open(self.moods_filepath, mode='r', encoding='utf-8') as jsonfile:
                    moods = json.load(jsonfile)
                    new_mood = {
                        "id": self.mood_id + 1,
                        "name": name
                    }
                    moods.append(new_mood)
                with open(self.moods_filepath, mode='w', encoding='utf-8') as jsonfile:
                    json.dump(moods, jsonfile, indent=4)
                self.mood_id += 1  # update id for json
                return True
        except Exception as e:
            return False
        
    def view_all_moods(self):
        ''' A function the returns all moods in the json file '''
        mood_names = []
        try:
            with open(self.moods_filepath, mode='r', encoding='utf-8') as jsonfile:
                moods = json.load(jsonfile)
                for mood in moods:
                    if 'name' in mood:
                        mood_names.append(mood['name'])
            return mood_names
        except Exception as e:
            return []