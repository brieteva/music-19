import grpc
from concurrent import futures
import my_proto_pb2_grpc
from auth_module import AuthService

def server():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    my_proto_pb2_grpc.add_AuthServiceServicer_to_server(AuthService(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    print('Server started.')
    server.wait_for_termination()
    print('Server stopped.')

if __name__ == '__main__':
    server()
