import grpc
from auth_logic import validate_credentials, register_user
import my_proto_pb2_grpc
import my_proto_pb2

class AuthService(my_proto_pb2_grpc.AuthServiceServicer):
    def Login(self, request, context):
        '''Checks if the given username and password match an entry in the user data and returns a message if successful/failed.'''
        success = validate_credentials(request.username, request.password)
        if success:
            return my_proto_pb2.LoginResponse(success=True, message="Login successful")
        else:
            return my_proto_pb2.LoginResponse(success=False, message="Invalid credentials")

    def Register(self, request, context):
        '''Registers a new user and returns a message if successful/failed.'''
        success = register_user(request.username, request.password)
        if success:
            return my_proto_pb2.RegisterResponse(success=True, message="Registration successful")
        else:
            return my_proto_pb2.RegisterResponse(success=False, message="Registration failed")
        
class AuthClient:
    def __init__(self):
        '''Initializes the client stub.'''
        channel = grpc.insecure_channel('localhost:50051')
        self.stub = my_proto_pb2.AuthServiceStub(channel)

    def login(self, username, password):
        '''Sends a login request to the server and returns a message if successful/failed.'''
        request = my_proto_pb2.LoginRequest(username=username, password=password)
        response = self.stub.Login(request)
        return response.success, response.message

    def register(self, username, password):
        '''Sends a register request to the server and returns a message if successful/failed.'''
        request = my_proto_pb2.RegisterRequest(username=username, password=password)
        response = self.stub.Register(request)
        return response.success, response.message