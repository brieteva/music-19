import csv

def validate_credentials(username, password):
    ''' A function that validates the provided username and password'''
    user_data = load_user_data()
    for entry in user_data:
        if entry['username'].lower() == username.lower() and entry['password'] == password:
            return True
    return False

def register_user(username, password):
    '''Function to register a new user'''
    if not is_username_valid(username):
        return False
    user_data = load_user_data()
    user_data.append({'username': username, 'password': password})
    save_user_data(user_data)
    return True

def load_user_data():
    '''Function to load user data from a file or database'''
    user_data = []
    print("Loading user data...")
    with open('src/files/user_data.csv', mode='r') as file:
        reader = csv.reader(file)
        for row in reader:
            if len(row) == 2:
                username, password = row
                user_data.append({'username': username, 'password': password})
    return user_data

def save_user_data(user_data):
    '''Function to save user data to a file or database'''
    with open('src/files/user_data.csv', mode='w', newline='') as file:
        writer = csv.writer(file)
        for entry in user_data:
            writer.writerow([entry['username'], entry['password']])

def is_username_valid(username):
    '''Function to validate username format'''
    if '@ru.is' not in username:
        return False
    return True