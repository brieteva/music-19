from src.logic.song import Song

class SongView(Song):
    def __init__(self) -> None:
        '''Initializes the song view'''
        super().__init__()
        self.WIDTH = 20
        self.status = 'playing'

    def play(self):
        '''Play song'''
        self.status = 'playing'
    
    def pause(self):
        '''Pause song'''
        self.status = 'paused'

    def next(self):
        '''Skip song and play next'''
        return 'next'

    def previous(self):
        '''Play previous song or start song over if song has played for a bit.'''
        return 'previous'
    
    def milliseconds_to_minutes_seconds(self, milliseconds):
        '''Convert milliseconds to minutes and seconds and return as a string in "m:ss" format.'''
        seconds = milliseconds // 1000
        
        # Calculate minutes and seconds
        minutes = seconds // 60
        seconds = seconds % 60
        
        # Format the result as a string in "m:ss" format
        result = f"{minutes}:{seconds:02}"
        
        return result

    # display functions
    def time_played(self):
        '''Return the play bar'''
        position = int(round(self.timePlayed / self.duration, 1) * self.WIDTH)
        position = self.WIDTH if position > self.WIDTH else position
        bar = "─"*position + "〇" + "─"*(self.WIDTH - position)
        return bar
    
    def get_state_of_button(self):
        '''Return the state of the play button'''
        return "II" if (self.status == 'playing') else "▷"
    
    def now_playing(super):
        ''' A function that prints out the song playing'''
        print(f"now playing {super.name} by {super.artist}")

    def __str__(self) -> str:
        '''Return the song view as a string'''
        bar = self.time_played()
        duration = self.milliseconds_to_minutes_seconds(self.duration)
        current_duration = self.milliseconds_to_minutes_seconds(self.timePlayed)
        play_button = self.get_state_of_button()
        if self.status == 'playing':
            self.add_time(1000)
        return """
            » {1} {2} «
            {4} {0} {3}
                   ⇄   ◃◃   {5}   ▹▹   ↻
            """.format(
                    bar, 
                    self.name, 
                    self.artist, 
                    duration, 
                    current_duration, 
                    play_button
                    )
    