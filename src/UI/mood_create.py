from src.logic.moods import Moods

def moods_main():
    '''Main function for the playlist creation process. Prompts the user for input and uses the Playlists class to create a playlist.'''
    moods = Moods()

    while True:
        print("\nMood Creation")
        print("Enter 'q' to quit.")

        name = input("Enter Mood name: ")
        if name == 'q':
            break

        created = moods.create_mood(name)
        if created:
            print(f"Playlist '{name}' created successfully!")
            break
        else:
            print("Failed to create Mood. Please check your input.")

if __name__ == "__main__":
    moods_main()
