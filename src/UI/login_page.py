from src.UI.main_menu import MainMenuUi
import csv

class LoginPage():
    def __init__(self):
        '''Initializes the login page and loads the user data.'''
        self.main_page = MainMenuUi()
        self.user_data = self.load_user_data()

    def login_options(self):
        '''Displays the login options to the user.'''
        print("*" * 30)
        print("Enter Username and Password")
        print()
        print("1. Login")
        print("2. Register")
        print()
        print("q. Quit")

    def load_user_data(self):
        '''Loads the user data from the csv file and returns it as a list of dictionaries.'''
        user_data = []
        with open('src/files/user_data.csv', mode='r') as file:
            reader = csv.reader(file)
            for row in reader:
                if len(row) == 2:
                    username, password = row
                    user_data.append({'username': username, 'password': password})
        return user_data

    def input_prompt(self):
        '''Prompts the user for input and handles their choice.'''
        while True:
            self.login_options()
            choice = input("Enter your choice ('1' for Login, '2' for Register, 'q' for Quitting): ")
            if choice == '1':
                user_input = input("Input Username: ").lower()
                if not self.validate_username(user_input):
                    print("Invalid username, try again!")
                    continue

                password_input = input("Input Password: ")
                if self.validate_credentials(user_input, password_input):
                    self.main_page.input_prompt()
                    break
                else:
                    print("Invalid password, try again!")
            elif choice == '2':
                self.register_user()
            elif choice == "q":
                print(F"Quitting program")
                break
            else:
                print("Invalid choice. Please select 1 for Login, 2 for Register or q for Quit.")

    def refresh_user_data(self):
        '''Reloads the user data from the csv file.'''
        self.user_data = self.load_user_data()

    def validate_username(self, username):
        '''Checks if the given username exists in the user data.'''
        return any(entry['username'].lower() == username.lower() for entry in self.user_data)

    def validate_credentials(self, username, password):
        '''Checks if the given username and password match an entry in the user data.'''
        return any(entry['username'].lower() == username.lower() and entry['password'] == password for entry in self.user_data)


    def register_user(self):
        '''Handles user registration, adding the new user to the user data csv file.'''
        new_username = input("Enter your email: ").lower()
        if self.validate_username(new_username):
            print("Username already exists. Please choose a different one.")
            new_username = input("Enter your email: ").lower()
        if '@ru.is' not in new_username:
            print()
            print("The email is not a part of RU and can not be used with this application")
            return

        new_password = input("Enter a new password: ")

        csv_file_path = 'src/files/user_data.csv'
        with open(csv_file_path, mode='a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow([new_username, new_password])
        self.refresh_user_data()
        print("Registration successful!")


if __name__ == "__main__":
    i = LoginPage()
    i.input_prompt()
