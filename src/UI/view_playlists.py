import sys
print(sys.path)
from src.logic.playlist import Playlists

def playlists_main():
    """Select playlist to inspect"""
    print()
    print('****All Playlists****')
    print()
    playlists_instance = Playlists()
    playlists = playlists_instance.view_all_playlists()
    counter = 1
    if playlists:
        for playlist in playlists:
            print(f"{counter}. " + playlist)
            counter += 1
            print()
    print()
    print("q. Quit")
    print("*" * 30)
    while True:
        chosen_option = input("choose option: ")
        if chosen_option == "q".lower():
            break
        elif int(chosen_option) <= counter and int(chosen_option) > 0:
            display_songs(playlists[int(chosen_option)-1])
            break
        else:
            print("Invalid choice. Please enter a valid option.")

    

def display_songs(selected_playlist):
    """displays all songs in playlist"""
    songs = Playlists().get_songs_in_playlist(selected_playlist)
    print()
    print('****Songs in Playlist****')
    for song in songs:
        print(song["name"])
        print()
    print("*" * 30)
    while True:
        chosen_option = input("Press q to go back: ")
        if chosen_option == "q".lower():
            playlists_main()
            break
        else: print("Invalid choice. Please enter a valid option.")