from src.logic.playlist import Playlists

def main():
    '''Main function for the playlist creation process. Prompts the user for input and uses the Playlists class to create a playlist.'''
    playlists = Playlists()

    while True:
        print("\nPlaylist Creation")
        print("Enter 'q' to quit.")

        name = input("Enter playlist name: ")
        if name == 'q':
            break

        genre = input("Enter playlist genre (techno/pop/jazz/rock): ")
        if genre == 'q':
            break
        while genre not in playlists.genre:
            print('This genre does not exist')
            genre = input("Enter playlist genre (techno/pop/jazz/rock): ")

        mood = input("Enter playlist mood (optional): ")
        if mood == 'q':
            break
        if mood not in playlists.mood:
            playlists.mood.append(mood)

        created = playlists.create_playlist(name, genre, mood)
        if created:
            print(f"Playlist '{name}' created successfully!")
            break
        else:
            print("Failed to create playlist. Please check your input.")

if __name__ == "__main__":
    main()
