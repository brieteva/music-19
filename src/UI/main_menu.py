'''import playlist_UI'''
from src.logic.playlist import Playlists
from src.UI.playlist_create import main
from src.UI.view_playlists import playlists_main
from src.logic.song import Song
from src.logic.moods import Moods
from src.UI.mood_create import moods_main
from src.logic.weather import Weather
from src.UI.song_view import *
import os
import time
class MainMenuUi:
    def __init__(self):
        '''Initializes the main menu.'''
        self.main_menu = None
        '''self.login_page = LoginPage'''
        '''self.logic_wrapper = Logic_Wrapper()'''
    
    def menu_output(self):
        '''A function that prints out the main menu logo and lists the options'''
        print("""\
                                                                                          
                                 .:::                  -.                                 
                          .=+*#%@@@*:                .#@%:                                
         .:=..             ..*@@@@%                   -@*                                 
        *@@@%=:               =@@@#    .:--=====--:.  -@#                                 
       .#@@@@%-:               *@@%*#@@@@@@@@@@@@@@@@%%@%.                                
        :#@@@@#-             .-.@@@@%@@@@@@@@@@@@@@@@@@@@@@*=.                            
         .*@@@@.          .=%@@:+@@*+#%%#*++===++*#%@@@@@@@@@@#-                          
           .+@@#        .*@@@@@# %@%:.#%=            .#@@@@@@@@@@+.                       
             -@@%.    .*@@@@@@@* =@@. %=%             *@@.-#@@@@@@@*                      
              :%@%:  +@@@@@@%=    @@= +*%-            #@@   .+@@@@@@@-                    
               :%@@=:@@@@@@=      +@%::@##            %@@.     +@@@@@@*                   
                 #@@+:%@@#.       .@@=.#+@:       -*%@@@@@%#+.  :%@@@@@#                  
                 .@@@#:#+          #@* -%+*     -%@@@@@@@@@@@@+   *@@@@@#                 
                 #@@@@%:           =@@-=@#@%@%..@@@@@@@@@@@@@@@-   *@@@@@+                
                =@@@@@@@-         -=%@@%##@%%%+.@@@@@@@@@@@@@@@=    %@@@@@:               
                %@@@@@@@@=         +*@@@##@%#%% #@@@@@@@@@@@@@%     :@@@@@*               
               :@@@@@*:%@@*        ==@%#+=@@#.  +%@@@@@@@@@@@@#      %@@@@@               
               =@@@@@- .%@@%=     -. *#*  #@@     *@@@@@@@@@%        +@@@@@:              
               +@@@@@:  :@@@@#.  -@@::@#. -@@-    =@@@@@@@@@%        =@@@@@-              
               =@@@@@- #@@@@@@@%%@@@+ ##+  -#=    *@@@@@@@@@@.       +@@@@@:              
      ::.      :@@@@@+*@@@@@@@@@@@@@%.:%#   -# ::*@@@@@@@@@@@#-:.    #@@@@@       ::.     
       -**+=-:..%@@@@@#@@@@@@@@@@@@@@@#++-  .@.#@@@@@@@@@@@@@@@@.   :@@@@@#::-=+*#+.      
        .+#########%@@*=%@@@@@@@@@@@@@@@@@@*---@@@@@@@@@@@@@@@@@#   %@%#########*:        
          :*####+#@%###= :*@@@@@@@@@@@@@@@@@@#.#@@@@@@@@@@@@@@@@@- +##%@@*#####=          
        -+*#####-:%%%%@@=  %@@@@@@@@@@@@@@@@@@%.%@@@@@@@@@@@@@@@@+*@%%%%#:######+-:       
      -+*#######-.=######*+###%%%%%%%%%%%%%%%%%=*%%%%%%%%%%%#####*######..#######*+=.    
                 __  __                    _   __  __              _       
                |  \/  |                  | | |  \/  |            (_)      
                | \  / |  ___    ___    __| | | \  / | _   _  ___  _   ___ 
                | |\/| | / _ \  / _ \  / _` | | |\/| || | | |/ __|| | / __|
                | |  | || (_) || (_) || (_| | | |  | || |_| |\__ \| || (__ 
                |_|  |_| \___/  \___/  \__,_| |_|  |_| \__,_||___/|_| \___|
                                                                                          
                                                                                                                                       
                                                                                                    
""")
        print("*" * 30)
        print("1. Playlists")
        print("2. Songs")
        print("3. Moods")
        print("4. Weather")
        print()
        print("q. Quit")
        print("*" * 30)

    def songs_promt(self):
      '''A function that lists the inputs for the songs'''
      while True:          
        print("*" * 30)
        print()
        print('*****Playlists*****')
        print()
        print("1. play song")
        print("2. ???")
        print("3. View all songs")
        print()
        print("q. Quit")
        print("*" * 30)
        choice = input("choose option for songs: ").lower()
        print()
        if choice == "q":
              break
        elif choice == "1":
          print('****Play song****')
          try:
            song_instance = Song()
            songs = song_instance.view_all_songs()
            song_view_instance = SongView()
            while True:
              print(song_view_instance.now_playing())
              print(song_view_instance)
              time.sleep(2)
              os.system('cls' if os.name == 'nt' else 'clear')
          except Exception as e:
            print(f"An exception occured: {e}")
        elif choice == "2":
            pass
        elif choice == "3":
          print()
          print('****All Songs****')
          print()
          song_instance = Song()
          songs = song_instance.view_all_songs()
          if songs:
            for song in songs:
              print(song)
              print()

    def playlists_promt(self):
      '''A function that lists input for playlists'''
      while True:          
        playlists_instance = Playlists()
        print("*" * 30)
        print()
        print('*****Playlists*****')
        print()
        print("1. Create Playlist")
        print("2. Edit Playlist")
        print("3. View all Playlists")
        print("4. Add songs to Playlist")
        print()
        print("q. Quit")
        print("*" * 30)
        choice = input("choose option for playlists: ").lower()
        print()
        if choice == "q":
              break
        elif choice == "1":
          main()
          print()
        elif choice == "2":
            playlists = playlists_instance.view_all_playlists()

            if playlists:
                for index, playlist in enumerate(playlists, 1):
                    print(f"{index}. {playlist}")
                    print()
                
                try:
                    choice = int(input("Select a playlist above: "))
                    
                    if 1 <= choice <= len(playlists):
                        selected_playlist = playlists[choice - 1]
                        while True:
                          print(f"You selected: {selected_playlist}")
                          print()
                          print(f"1. Change name of {selected_playlist}")
                          print()
                          print(f"2. Change genre of {selected_playlist}")
                          print()
                          print(f"3. Change mood of {selected_playlist}")
                          choice_for_edit = input("What do you want to edit? ").lower()
                          print()
                          if choice_for_edit == "1":
                                print()
                                print("Here you can change the name")
                                new_name = input(f"What do you want the new name for {selected_playlist} to be? ")
                                playlists_instance.change_playlist_name(selected_playlist, new_name)
                                break
                          elif choice_for_edit == "2":
                                print()
                                print("Here you can change the genre")
                                json_file = 'src/files/playlists.json'
                                allowed_genres = ['pop', 'rock', 'jazz', 'techno']
                                new_genre = input(f'What do you want the new genre to be out of the following: (Techno, Pop, Jazz, Rock) ').lower()
                                if new_genre not in allowed_genres:
                                    print("Not a supported genre")
                                else:
                                  playlists_instance.change_playlist_genre(selected_playlist, new_genre)
                                  break
                          elif choice_for_edit == "3":
                                print()
                                print("Here you can change the mood")
                                json_file = 'src/files/playlists.json'
                                new_mood = input(f'What do you want the new mood to be?')
                                playlists_instance.change_playlist_mood(selected_playlist, new_mood)
                                break
                          elif choice_for_edit == "q":
                                break
                          else:
                                print("Invalid choice. Please enter a valid option.")
                except ValueError:
                    print("Invalid input. Please enter a number.")
            else:
                print("No playlists found.")

        elif choice == "3":
            playlists_main()
        elif choice == "4":
            print()
            print('****Add song to playlist****')
            print()

            # Load songs and playlists
            songs = playlists_instance.load_songs()
            playlists = playlists_instance.load_playlists()

            if playlists:
                for index, playlist in enumerate(playlists, 1):  # Start the numbering at 1
                    print(f"{index}. {playlist['name']} ({len(playlist['songs'])} songs)")
                    print()
                
                try:
                    playlist_choice = int(input("Select a playlist above: "))
                    
                    if 1 <= playlist_choice <= len(playlists):
                        selected_playlist = playlists[playlist_choice - 1]
                        print(f"You selected: {selected_playlist['name']}")
                        print("Available songs:")
                        
                        for index, song in enumerate(songs, 1):
                            print(f"{index}. {song['name']} by {song['artist']}")
                        
                        try:
                            song_choice = int(input("Select a song to add to the playlist: "))
                            
                            if 1 <= song_choice <= len(songs):
                                selected_song = songs[song_choice - 1]
                                selected_playlist['songs'].append(selected_song)
                                
                                # Update the playlists.json file
                                playlists_instance.save_playlists(playlists)
                                print(f"{selected_song['name']} added to {selected_playlist['name']} playlist.")
                            else:
                                print("Invalid choice. Please enter a valid index.")
                        except ValueError:
                            print("Invalid input. Please enter a number.")
                    else:
                        print("Invalid choice. Please enter a valid index for the playlist.")
                except ValueError:
                    print("Invalid input. Please enter a number for the playlist.")
            else:
                print("No playlists found.")
            print()
    
      # TODO Tengja þetta saman
      def mood_promt(self, sorted_data):
        for song in sorted_data:
          print(f"Song: {song['name']}")
          print(f"Artist: {song['artist']}")
          print(f"Duration: {song['duration']} seconds")
          print(f"Mood: {song['mood']}")
          print(f"Genre: {song['genre']}\n")

    def mood_promt(self):
          '''A function that lists the inputs for the songs'''
          while True:          
            print("*" * 30)
            print()
            print('*****Moods*****')
            print()
            print("1. Create new mood")
            print("2. View all moods")
            print()
            print("q. Quit")
            print("*" * 30)
            choice = input("choose option for moods: ").lower()
            if choice == "q":
                  break
            elif choice == "1":
                moods_main()
            elif choice == "2":
              print()
              moods_instance = Moods()
              moods = moods_instance.view_all_moods()
              if moods:
                for mood in moods:
                  print(mood)
              print()
            
    def weather_promt(self):
      '''A function that lists the inputs for the weather'''
      while True:          
        print("*" * 30)
        print()
        print('*****Weather*****')
        print()
        print("1. View current weather")
        print("2. View four day forecast")
        print("3. View historical forecast")
        print()
        print("q. Quit")
        print("*" * 30)
        choice = input("choose option for weather: ").lower()
        if choice == "q":
              break
        elif choice == "1":
            try:
                weather_instance = Weather()
                weather_instance.get_current_weather(weather_instance.location, weather_instance.API_KEY)
                print('Current weather in Reykjavík' + weather_instance.weather + 'is' + '\n' + 'Mood: ' + weather_instance.get_mood(weather_instance.weather))
            except Exception as e:
                print(f"An exception occured: {e}")
        elif choice == "2":
            try:
                weather_instance = Weather()
                weather_instance.get_four_day_forecast(weather_instance.location, weather_instance.API_KEY)
                print('Four day forecast for Reykjavík' + '\n' + 'Mood: ' + weather_instance.get_mood(weather_instance.weather))
            except Exception as e:
                print(f"An exception occured: {e}")
        elif choice == "3":
            pass
      
      
    def input_prompt(self):
      '''A function that lists all available inputs for the main menu'''
      while True:
          self.menu_output()
          choice = input("Choose option: ").lower()
          if choice == "q":
                print("Quitting program")
                break
          elif choice == "1":
              self.playlists_promt()
          elif choice == "2": #song promt
              self.songs_promt()
          elif choice == "3": #mood promt
            self.mood_promt()
          elif choice == "4": #weather promt
              self.weather_promt()
          else:
                print("Invalid choice, try again!")





