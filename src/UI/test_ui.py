import unittest
import sys
import csv
from io import StringIO

from main_menu import MainMenuUi
from UI.login_page import LoginPage

from unittest.mock import patch, call
from unittest.mock import patch, mock_open

class TestMainMenu(unittest.TestCase):
    @patch('builtins.input', side_effect=['q'])
    @patch('builtins.print')
    def test_input_prompt_quit_1(self, mock_print, mock_input):
        '''Checks whether the program prints the right output message given the correct input in lowercase'''
        obj = MainMenuUi()
        obj.input_prompt()
        mock_print.assert_called_with("Quitting program")

    @patch('builtins.input', side_effect=['Q'])
    @patch('builtins.print')
    def test_input_prompt_quit_2(self, mock_print, mock_input):
        '''Checks whether the program printns the right output message given the correct input in uppercase'''
        obj = MainMenuUi()
        obj.input_prompt()
        mock_print.assert_called_with("Quitting program")

    @patch('builtins.input', side_effect=['random_input', 'q'])
    @patch('builtins.print')
    def test_input_prompt_invalid_then_quit(self, mock_print, mock_input):
        '''Checks whether the program prints the right output given an incorrect input'''
        '''Then check if the program quits'''
        obj = MainMenuUi()
        obj.input_prompt()
        calls = [call("Invalid choice, try again!"), call("Quitting program")]
        mock_print.assert_has_calls(calls, any_order=True)
    
    
    @patch('builtins.print')
    def test_print_statements(self, mock_print):
        '''Test that checks whether the program prints the right number of print statements'''
        menu = MainMenuUi() 
        menu.menu_output()
        print_calls = mock_print.call_count
        expected_print_calls = 7
        self.assertEqual(print_calls, expected_print_calls)

class TestLoginPage(unittest.TestCase):

    def test_case1(self): #happy path
        '''Test if a valid email is accepted in the system is recognized as valid'''
        logintest = LoginPage()
        result = logintest.validate_username('snorri22@ru.is')
        self.assertEqual(result,True)

    def test_case3(self): 
        '''Test if a specific username is in the CSV file'''
        username_to_check = 'snorri@ru.is'
        
        with open('files/user_data.csv', 'r') as csv_file:
            csv_reader = csv.reader(csv_file)
            for row in csv_reader:
                if row[0] == username_to_check:
                    result = False
                    break
            else:
                result = False

        self.assertTrue(result)

    def test_case2(self): 
        '''Test if an invalid email is accepted in the system is recognized as invalid'''
        logintest = LoginPage()
        result = logintest.validate_username('snorri22@gmail.is')
        self.assertEqual(result,False)    

    def test_case3(self): 
        '''Test if correct credentials are validated correctly'''
        logintest = LoginPage()
        result = logintest.validate_credentials('snorri22@ru.is', 'snorri')
        self.assertEqual(result,True)
    
    @patch('builtins.print')
    def test_print_statements(self, mock_print):
        '''Test that checks whether the program prints the right number of print statements'''
        menu = LoginPage() 
        menu.login_options()
        print_calls = mock_print.call_count
        expected_print_calls = 7
        self.assertEqual(print_calls, expected_print_calls)
    
    def test_load_user_data(self):
        '''Test that checks whether the program loads the user data from the csv file and returns it as a list of dictionaries'''
        menu = LoginPage()
        user_data = menu.load_user_data()
        self.assertIsInstance(user_data, list)

class TestLoginPage(unittest.TestCase):

    def setUp(self):
        # Create an instance of LoginPage before each test
        self.login_page = LoginPage()

    def capture_output(self, function_to_test, *args):
        '''Capture the output of a function that prints to stdout and return it as a string.'''
        captured_output = StringIO()
        sys.stdout = captured_output
        try:
            function_to_test(*args)
            return captured_output.getvalue().strip()
        finally:
            # Restore stdout to its original state
            sys.stdout = sys.__stdout__

    def test_login_options_output(self):
        '''Test if the login options menu outputs as expected.'''
        # Define the expected output
        expected_output = """
******************************
Enter Username and Password

1. Login
2. Register

q. Quit
""".strip()

        # Capture the output of the login_options method
        output = self.capture_output(self.login_page.login_options)

        # Assert that the captured output matches the expected output
        self.assertEqual(output, expected_output)

    def test_load_user_data_valid(self):
        '''Test if the user data is loaded correctly from a mock CSV file.'''
        csv_content = "snorri22@ru.is,password\nalice@ru.is,secret\nbob@ru.is,123456\n"

        # Mock the 'open' function to return the mock CSV content
        with patch('builtins.open', mock_open(read_data=csv_content)) as mock_file:
            login_page = LoginPage()
            user_data = login_page.load_user_data()

        # Ensure the user_data contains the expected entries
        expected_data = [
            {'username': 'snorri22@ru.is', 'password': 'password'},
            {'username': 'alice@ru.is', 'password': 'secret'},
            {'username': 'bob@ru.is', 'password': '123456'},
        ]
        self.assertEqual(user_data, expected_data)

    def test_load_user_data_empty_file(self):
        '''Test if an empty CSV file results in an empty user_data list.'''
        csv_content = ""

        # Mock the 'open' function to return the mock CSV content
        with patch('builtins.open', mock_open(read_data=csv_content)) as mock_file:
            login_page = LoginPage()
            user_data = login_page.load_user_data()

        # Ensure the user_data is an empty list
        self.assertEqual(user_data, [])


    def test_validate_existing_username(self):
        '''Test if an existing username is recognized as valid.'''
        logintest = LoginPage()
        result = logintest.validate_username('snorri22@ru.is')
        self.assertTrue(result)

    def test_validate_nonexistent_username(self):
        '''Test if a nonexistent username is recognized as invalid.'''
        logintest = LoginPage()
        result = logintest.validate_username('nonexistent@ru.is')
        self.assertFalse(result)

    def test_validate_existing_credentials(self):
        '''Test if existing credentials are validated successfully.'''
        logintest = LoginPage()
        result = logintest.validate_credentials('snorri22@ru.is', 'snorri')
        self.assertTrue(result)

    def test_validate_invalid_credentials(self):
        '''Test if invalid credentials are recognized as invalid.'''
        logintest = LoginPage()
        result = logintest.validate_credentials('snorri22@ru.is', 'invalid_password')
        self.assertFalse(result)

    def test_register_existing_username(self):
        '''Test if an existing username is rejected when registering a new user.'''
        logintest = LoginPage()
        with patch('builtins.input', side_effect=['snorri22@ru.is', 'password']):
            with patch('builtins.print') as mock_print:
                logintest.register_user()
                mock_print.assert_called_with("Username already exists or invalid. Please choose a different one.")

    def test_register_invalid_username(self):
        '''Test if an invalid username is rejected when registering a new user.'''
        logintest = LoginPage()
        with patch('builtins.input', side_effect=['invalid_username', 'password']):
            with patch('builtins.print') as mock_print:
                logintest.register_user()
                mock_print.assert_called_with("Username already exists or invalid. Please choose a different one.")


    def setUp(self):
        # Create an instance of LoginPage before each test
        self.login_page = LoginPage()
        # Example test data for user_data
        self.login_page.user_data = [
            {'username': 'snorri22@ru.is', 'password': 'password'},
            {'username': 'alice@ru.is', 'password': 'secret'},
            {'username': 'bob@ru.is', 'password': '123456'},
        ]

    @patch('builtins.open', mock_open(read_data="snorri22@ru.is,password\nalice@ru.is,secret\nbob@ru.is,123456\n"))
    def test_load_user_data_valid_with_mock(self):
        '''Test if the user data is loaded correctly from a mock CSV file.'''
        user_data = self.login_page.load_user_data()

        expected_data = [
            {'username': 'snorri22@ru.is', 'password': 'password'},
            {'username': 'alice@ru.is', 'password': 'secret'},
            {'username': 'bob@ru.is', 'password': '123456'},
        ]
        self.assertEqual(user_data, expected_data)

    # Add other test cases for different scenarios as needed


    def test_validate_credentials_valid(self):
        '''Test if valid credentials are validated successfully.'''
        result = self.login_page.validate_credentials('snorri22@ru.is', 'password')
        self.assertTrue(result)

    def test_validate_credentials_invalid_password(self):
        '''Test if a valid username with an incorrect password is rejected.'''
        result = self.login_page.validate_credentials('snorri22@ru.is', 'invalid_password')
        self.assertFalse(result)

    def test_validate_credentials_invalid_username(self):
        '''Test if an invalid username is rejected, regardless of password.'''
        result = self.login_page.validate_credentials('nonexistent@ru.is', 'password')
        self.assertFalse(result)

    def test_validate_credentials_case_insensitive(self):
        '''Test if the username is case-insensitive.'''
        result = self.login_page.validate_credentials('SnOrRi22@ru.is', 'password')
        self.assertTrue(result)

    def test_validate_credentials_empty_data(self):
        '''Test if an empty user_data list results in a failed validation.'''
        self.login_page.user_data = []
        result = self.login_page.validate_credentials('anyusername@ru.is', 'password')
        self.assertFalse(result)


    @patch('builtins.print')
    def test_login_options_menu(self, mock_print):
        '''Test if the login options menu is displayed in the expected format.'''
        menu = LoginPage()
        menu.login_options()
        menu_expected = [
            call("******************************"),
            call("Enter Username and Password"),
            call(),
            call("1. Login"),
            call("2. Register"),
            call(),
            call("q. Quit")
        ]
        mock_print.assert_has_calls(menu_expected, any_order=False)
    
if __name__ == '__main__':
    unittest.main()
